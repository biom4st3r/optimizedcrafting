package com.biom4st3r.recipecache.mixin;

import java.util.Optional;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.CraftingResultInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.s2c.play.ScreenHandlerSlotUpdateS2CPacket;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.screen.AbstractRecipeScreenHandler;
import net.minecraft.screen.CraftingScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;

@Mixin(CraftingScreenHandler.class)
public abstract class CraftingScreenHandlerMxn extends AbstractRecipeScreenHandler<CraftingInventory> {

    public CraftingScreenHandlerMxn(ScreenHandlerType<?> screenHandlerType, int i) {
        super(screenHandlerType, i);
    }

    @Shadow @Final
    private CraftingInventory input;
    @Shadow @Final
    private CraftingResultInventory result;
    @Shadow @Final
    private PlayerEntity player;

    @Unique
    Optional<CraftingRecipe> lastRecipe = Optional.empty();

    @Redirect(
        at = @At(value = "INVOKE", target = "net/minecraft/screen/CraftingScreenHandler.updateResult(ILnet/minecraft/world/World;Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/inventory/CraftingInventory;Lnet/minecraft/inventory/CraftingResultInventory;)V"),
        method = "method_17401"
    )
    private void cancelUpdate(int syncId, World world, PlayerEntity player, CraftingInventory craftingInventory, CraftingResultInventory resultInventory)
    {
        if(!world.isClient)
        {
            ItemStack stack = ItemStack.EMPTY;
            ServerPlayerEntity spe = (ServerPlayerEntity) player;
            if(!this.input.isEmpty())
            {
                if(!this.lastRecipe.isPresent() || !this.lastRecipe.get().matches(input, world) )
                {
                    this.lastRecipe = world.getServer().getRecipeManager().getFirstMatch(RecipeType.CRAFTING, input, world);
                }
                if(this.lastRecipe.isPresent())
                {
                    CraftingRecipe recipe = lastRecipe.get();
                    if(this.result.shouldCraftRecipe(world, spe, recipe))
                    {
                        stack = recipe.craft(this.input);
                    }
                }
            }
            this.result.setStack(0, stack);
            spe.networkHandler.sendPacket(new ScreenHandlerSlotUpdateS2CPacket(this.syncId, 0, stack));
        }
    }

}
